%\section{Literature \& State-of-the-Art}
\label{sec:literature_soa}

The issue of energy and power consumption has been extensively explored by the
research and practitioner HPC community. The problem has been tackled from many
angles, ranging from technological solutions (energy efficient devices) to
careful resource management (DVFS, RAPL) and software approaches (power-aware
workload scheduling techniques), and many more\cite{maiterth2017power}.

In this section we present an overview of some of the techniques from the
literature, focusing on two main areas: (1) methods that aim explicitly at
maintaining the power consumption of a supercomputer within a power budget,
and (2) methods that target the broader issue of energy efficiency.

\subsection{Power Capping}
\label{sec:literature_soa_powerCap}

Several approaches for curtailing the power consumption of HPC systems
using \emph{power capping} have been proposed~\cite{lefurgy2008power}.
The typical challenge for these methods is finding the optimal
trade-off between constraining power consumption and maximizing system
utilization and users satisfaction.  The most common solutions range
from Dynamic Voltage and Frequency Scaling (DVFS)~\cite{Etinski2012579,lee2014power},
energy proportional systems~\cite{varsamopoulos2010energy},
over-provisioning~\cite{Patki:2013:EHO:2464996.2465009}, turning off
idle resources~\cite{hikita2008}, exploiting components
variability~\cite{shoukourian2015adviser}, optimizing the workload
placement, and scheduling placements of jobs and
tasks~\cite{meng2015simulation,cp_powerCap_borghesi15}.

Table~\ref{tab:lit_survey_power} summarizes some of the methods proposed in the
research literature in the last few years. We will not describe each method in
detail as our coverage is not a survey and we are more interested in observing overall
patterns. For this reason the Table is divided into six columns with the following
types of information for each column: 
\begin{itemize}
    \item \emph{Reference}, the paper describing the approach;
    \item \emph{Scale}, the target level which can either be the HPC
    computer system (\emph{Sys.}) or the application (\emph{App.}),
    although some approaches combine the Sys. and the App.;
    \item \emph{Mechanism}, the hardware or software tools used as power knobs;
    \item \emph{Evaluation}, the metrics used to measure the quality of the
        proposed solution;
    \item \emph{Job Type}, the flexibility of the application, which
    is rigid (\emph{R}), moldable (\emph{Mo}), malleable (\emph{Ma})
    or a mixture;
    \item \emph{Study Type}, a measure of the maturity of the proposed approach.
        Exploratory works (\emph{E}) present preliminary findings and exploratory
        analysis, theoretical models, without necessarily describing algorithms
        or solutions but rather identifying challenges and directions.
        Simulation works (\emph{S}) provide practical solutions and methodologies
        that have not been tested on real machines yet, but only on an HPC
        simulation framework.  Real approaches (\emph{R}) involve models that have been
        deployed and tested on actual supercomputers (not necessarily
        in-production).
\end{itemize}

We divided the types of HPC applications in three classes: (1) rigid, jobs with a
fixed set of resources that cannot be changed; (2) moldable, more flexible jobs
that have a resource set that can be changed before it is dispatched but must remain
stable during the execution; and (3) malleable, jobs whose resource set can
dynamically change.

We identified some common groups of mechanisms used to maintain the power
consumption under a given budget. Most proposed techniques and algorithms fall
into one of these types: dynamic power resource management such as \emph{DVFS}
(Dynamic Voltage and Frequency Scaling) and \emph{RAPL} (Intel Running Average Power
Limit); dynamic or static tuning of the job configuration (\emph{Config}), in
terms of nodes and threads allocated; \emph{Task Scheduling}, i.e. deciding the
execution order of the tasks composing a larger HPC application; job-level
scheduling (\emph{Job Exec. Order}), deciding the execution order of dispatched
jobs; prediction models (\emph{Pred. Models}), used to characterize the
power/energy-wise behavior of a supercomputer (or its components) and its
workload; the allocation of a workload to the system resources \emph{Alloc.}
%turning-off idle nodes (\emph{Idle Nodes}); power/energy aware pricing schemes.

The metrics used to assess the quality of the proposed approach are
varied.  Some methods focus exclusively on aspects of the power cap
bound, hence they use the average power consumption (\emph{APC}),
system consumption, or component-wise consumption as the evaluation metric.
Other approaches also take into consideration the energy consumption of the application
(Energy-to-Solution, \emph{EtS}) and the application duration
(Time-to-Solution, \emph{TtS}).  A widespread metric is related to the
quality of service perceived by the users, usually measured as
response time (\emph{Resp.  Time}) and Bounded Slowdown (\emph{BSLD}),
defined by the ratio between the time spent waiting in the system and
the job runtime\footnote{$BLSD = max(\frac{wait\_time +
run\_time'}{max(\theta, run\_time)}, 1)$ where $wait\_time$ is the
time spent waiting for execution, $run\_time$ is the ``original''
duration, $run\_time'$ is the final duration (possibly changed due to
power reduction or frequency scaling), and $\theta$ is a threshold used to
avoid the bias of very short jobs on the average value.}.  The rate of
processed jobs per unit of time (\emph{Throughput}) is less
common. Finally, the quality of some approaches is measured in terms
of \emph{Performance Gain}, where a composite score is created by
considering multiple metrics and then compared with a baseline.

%\begin{table*}[bt]
%\label{tab:lit_survey_power}
%\caption{Literature Survey: Explicit Power Capping Focus}
%\small\sf\centering
%\begin{tabular}{ p{0.17\linewidth} p{0.12\linewidth} p{0.15\linewidth} 
%    p{0.18\linewidth} p{0.1\linewidth} p{0.14\linewidth} }
%  %\toprule
%  \emph{Reference} & \emph{Scale} & \emph{Mechanism} & \emph{Evaluation} &
%  \emph{Job Type} & \emph{Study Type} \\
%  %\midrule
%  \cite{hsu2005towards} & Sys. & DVFS & BSLD & R & Real \\
%  %\cite{rountree2007bounding} & App. & Task \nohyphens{Scheduling} + DVFS & Ets
%  %                            & R & Real \\
%  \cite{rountree2007bounding} & App. & Task Scheduling + DVFS & Ets
%                              & R & Real \\
%  \cite{Rountree:2009:AMD:1542275.1542340} & App. & DVFS & Ets + TtS & R & Real
%  \\
%  \cite{gandhi2009power} & Sys. & DVFS & Response Time & R & Exploratory \\
%  \cite{Etinski2010} & Sys. & DVFS & BSLD & R & Simulation \\
%  \cite{Etinski2010_utilDriven} & Sys. & DVFS & BSLD & R & Simulation \\
%  \cite{Etinski2012615} & Sys. & DVFS & BSLD & R & Simulation \\
%  \cite{Patki:2013:EHO:2464996.2465009} & Sys. & Config + RAPL & TtS & Mo & Real
%  \\
%  \cite{sarood2013optimizing} & Sys. & Config + RAPL & EtS & Mo & Real \\
%  \cite{Sarood:2014:MTO:2683593.2683682} & Sys. & Config + RAPL & Throughput &
%  Mo + Ma & Real \\
%  \cite{Bodas:2014:SPS:2689710.2689713} & Sys. & RAPL & APC & R & Real \\
%  \cite{Bailey:2014:ACS:2703009.2706736} & App. & Config & Performance Gain & Mo
%                                         & Real \\
%  \cite{Patki:2015:PRM:2749246.2749262} & Sys. & Config + RAPL & TtS + APC & Mo
%  + Ma & Real \\
%  \cite{cp_powerCap_borghesi15} & Sys. & Job Exec. Order + Alloc. & BSLD & R &
%  Simulation \\
%  \cite{Borghesi2016} & Sys. & Pred. Model & APC & R & Real \\
%  \cite{wallace2016data} & Sys. & Job Exec. Order & APC + Resp. Time & R &
%  Simulation \\
%  \cite{Sirbu2016} & Sys. & Pred. Model & APC & R & Real \\
%  \cite{maiterth2016power} & Sys. & RAPL & Performance Gain & R & Simulation \\
%  %\cite{bailey2015finding} & App. & Task \nohyphens{Scheduling} + Config &
%  \cite{bailey2015finding} & App. & Task Scheduling + Config &
%  Performance Gain & Mo & Exploratory \\
%  \cite{Ellsworth:2015:DPS:2807591.2807643} & Sys. & RAPL & APC & R & Real \\
%  \cite{Marathe2015} & Sys. & RAPL & TtS & Mo & Real \\
%  \cite{Wilde2015} & Sys. & Alloc. & APC & Mo & Exploratory \\
%  \cite{Inadomi:2015:AMI:2807591.2807638} & Sys. & DVFS + RAPL & APC + TtS & R 
%                                          & Real \\
%  \cite{borghesi2018scheduling} & Sys. & Job Exec. Order & BSLD & R &
%  Simulation \\
%  %\bottomrule
%\end{tabular}
%\end{table*}

\begin{table}[bt]
\label{tab:lit_survey_power}
\caption{Literature Survey: Explicit Power Capping Focus}
\small\sf\centering
\begin{tabular}{ p{0.1\linewidth} p{0.1\linewidth} p{0.18\linewidth} 
    p{0.18\linewidth} p{0.1\linewidth} p{0.1\linewidth} }
  %\toprule
  \emph{Ref.} & \emph{Scale} & \emph{Mechanism} & \emph{Evaluation} &
  \emph{Job Type} & \emph{Study Type} \\
  \midrule
  \cite{hsu2005towards} & Sys. & DVFS & BSLD & R & R \\
  \cite{rountree2007bounding} & App. & Task Scheduling + DVFS & Ets
                              & R & R \\
  \cite{Rountree:2009:AMD:1542275.1542340} & App. & DVFS & Ets + TtS & R & R
  \\
  \cite{gandhi2009power} & Sys. & DVFS & Response Time & R & E \\
  \cite{Etinski2010} & Sys. & DVFS & BSLD & R & S \\
  \cite{Etinski2010_utilDriven} & Sys. & DVFS & BSLD & R & S \\
  \cite{Etinski2012615} & Sys. & DVFS & BSLD & R & S \\
  \cite{Patki:2013:EHO:2464996.2465009} & Sys. & Config + RAPL & TtS & Mo & R
  \\
  \cite{sarood2013optimizing} & Sys. & Config + RAPL & EtS & Mo & R \\
  \cite{Sarood:2014:MTO:2683593.2683682} & Sys. & Config + RAPL & Throughput &
  Mo + Ma & R \\
  \cite{Bodas:2014:SPS:2689710.2689713} & Sys. & RAPL & APC & R & R \\
  \cite{Bailey:2014:ACS:2703009.2706736} & App. & Config & Performance Gain & Mo
                                         & R \\
  \cite{Patki:2015:PRM:2749246.2749262} & Sys. & Config + RAPL & TtS + APC & Mo
  + Ma & R \\
  \cite{cp_powerCap_borghesi15} & Sys. & Job Exec. Order + Alloc. & BSLD & R &
  S \\
  \cite{Borghesi2016} & Sys. & Pred. Model & APC & R & R \\
  \cite{wallace2016data} & Sys. & Job Exec. Order & APC + Resp. Time & R &
  S \\
  \cite{Sirbu2016} & Sys. & Pred. Model & APC & R & R \\
  \cite{maiterth2016power} & Sys. & RAPL & Performance Gain & R & S \\
  \cite{bailey2015finding} & App. & Task Scheduling + Config &
  Performance Gain & Mo & E \\
  \cite{Ellsworth:2015:DPS:2807591.2807643} & Sys. & RAPL & APC & R & R \\
  \cite{Marathe2015} & Sys. & RAPL & TtS & Mo & R \\
  \cite{Wilde2015} & Sys. & Alloc. & APC & Mo & E \\
  \cite{Inadomi:2015:AMI:2807591.2807638} & Sys. & DVFS + RAPL & APC + TtS & R 
                                          & R \\
  \cite{borghesi2018scheduling} & Sys. & Job Exec. Order & BSLD & R &
  S \\
  %\bottomrule
\end{tabular}
	
\end{table}

The majority of the methods rely on some sort of dynamic power management
via frequency scaling or RAPL. Configuration changes are used by those
methods that consider moldable and/or malleable jobs. A few techniques try to
improve task or job dispatching, although almost all of them are not mature
enough for the implementation on a real HPC system; this can be easily
understood if we consider that changing the job dispatching mechanism is a
disruptive change that risks to compromise the normal behavior and
availability of a whole supercomputer.

Almost all approaches are situated at the system level, probably because
significant improvements can be gained only by considering the whole
machine.  Many methods have been deployed and tested on real systems,
either in-production machines or in a prototype phase. Most of the
works have been tested at least under simulation, resulting in a
relatively mature field where practical concerns and engineering
challenges guide the research direction.

\subsection{Energy Efficiency}
\label{sec:literature_soa_energy_efficiency}

%% Generally speaking, techniques that focus on power capping trade-off power
%% consumption for performance or users satisfaction. In the last years, many
%% researchers focused instead on energy-efficiency techniques that do not aims
%% only at the reduction of power consumption.
This section presents methods from the recent literature that propose
mechanisms, techniques, and methodologies to improve energy
efficiency.  These methods are reported in
Table~\ref{tab:lit_survey_energy} following the same structure used
for the Table with power capping methods.

It is notable that, contrary to power capping solutions, energy
efficient approaches seldom employ frequency scaling or RAPL.  For
CPU-bound applications, simply reducing the power consumption via a
decrease in frequency tends to increase the execution time of HPC
applications\cite{Freeh:2007:AET:1263127.1263246} which are typically
CPU-bound. This increase in execution time often has the side effect
that the energy consumption of an application runing at reduced
frequency and power actually can be greater than the original energy
consumption.  Usually, this is mitigated by employing adaptive
strategies such as decreasing the power consumption only during
certain application phases.

%\begin{table*}[bt]
%\small\sf\centering
%\begin{tabular}{ p{0.17\linewidth} p{0.12\linewidth} p{0.15\linewidth}
%    p{0.17\linewidth} p{0.1\linewidth} p{0.14\linewidth} }
%  %\toprule
%  \emph{Reference} & \emph{Scale} & \emph{Mechanism} & \emph{Evaluation} &
%  \emph{Job Type} & \emph{Study Type} \\
%  %\midrule
%  \cite{mammela2012energy} & Sys. & Idle Nodes & EtS & R & Simulation
%  \\
%  \cite{hikita2008} & Sys. & Idle Nodes & \$ & R & Real \\
%  \cite{Yang:2013:IDP:2503210.2503264} & Sys. & Job Exec. Order & APC & R &
%  Simulation \\
%  \cite{zhou2013reducing} & Sys. & Job Exec. Order & \$ & R & Simulation \\
%  \cite{shoukourian2015adviser} & Sys. & Config & EtS + TtS + APC & Mo. &
%  Exploratory \\
%  \cite{ms3_borghesi} & Sys. & Job Exec. Order + Alloc. & BSLD + APC & R &
%  Simulation \\
%  \cite{raghu13} & Sys. & Job Exec. Order + DVFS & APC & R & Real \\
%  \cite{auweter14} & Sys. + App. & DVFS & EtS + TtS + APC & R & Real \\
%  \cite{durillo2014multi} & App. & Job Exec. Order & EtS + TtS & R & Simulation
%  \\
%  \cite{shoukourian2014predicting} & Sys. & Pred. Model & EtS + TtS + APC &
%  Mo. & Real \\
%  \cite{shoukourian2015predicting} & Sys. & Pred. Model & EtS + TtS + APC &
%  Mo. & Real \\
%  \cite{langer2015analyzing} & Sys. + App. & Config + RAPL & EtS + TtS & Mo &
%  Real \\
%  \cite{gomezMartin16} & Sys. & Job Exec. Order & EtS + TtS & R & Simulation \\
%  \cite{borghesi2018pricing} & Sys. & Pricing Schemes & \$ & R &
%  Exploratory \\
%  \cite{beneventi_cooling-aware_2016} & Sys. & Pred. Model + Task scheduling &
%  EtS & R & Exploratory \\
%  \cite{cesarini2018countdown} & App. & DVFS & EtS + TtS & R &
%  Real \\
%  %\bottomrule
%\end{tabular}
%\caption{Literature Survey: Energy Efficiency \& Cost Savings}
%\label{tab:lit_survey_energy}	
%\end{table*}

\begin{table}[bt]
\label{tab:lit_survey_energy}	
\caption{Literature Survey: Energy Efficiency \& Cost Savings}
\small\sf\centering
\begin{tabular}{ p{0.1\linewidth} p{0.1\linewidth} p{0.18\linewidth} 
    p{0.18\linewidth} p{0.1\linewidth} p{0.1\linewidth} }
  %\toprule
  \emph{Ref.} & \emph{Scale} & \emph{Mechanism} & \emph{Evaluation} &
  \emph{Job Type} & \emph{Study Type} \\
  %\midrule
  \cite{mammela2012energy} & Sys. & Idle Nodes & EtS & R & S
  \\
  \cite{hikita2008} & Sys. & Idle Nodes & \$ & R & R \\
  \cite{Yang:2013:IDP:2503210.2503264} & Sys. & Job Exec. Order & APC & R &
  S \\
  \cite{zhou2013reducing} & Sys. & Job Exec. Order & \$ & R & S \\
  \cite{shoukourian2015adviser} & Sys. & Config & EtS + TtS + APC & Mo. &
  E \\
  \cite{ms3_borghesi} & Sys. & Job Exec. Order + Alloc. & BSLD + APC & R &
  S \\
  \cite{raghu13} & Sys. & Job Exec. Order + DVFS & APC & R & R \\
  \cite{auweter14} & Sys. + App. & DVFS & EtS + TtS + APC & R & R \\
  \cite{durillo2014multi} & App. & Job Exec. Order & EtS + TtS & R & S
  \\
  \cite{shoukourian2015predicting} & Sys. & Pred. Model & EtS + TtS + APC &
  Mo. & R \\
  \cite{langer2015analyzing} & Sys. + App. & Config + RAPL & EtS + TtS & Mo &
  Real \\
  \cite{gomezMartin16} & Sys. & Job Exec. Order & EtS + TtS & R & S \\
  \cite{borghesi2018pricing} & Sys. & Pricing Schemes & \$ & R &
  E \\
  \cite{beneventi_cooling-aware_2016} & Sys. & Pred. Model + Task scheduling &
  EtS & R & E \\
  \cite{cesarini2018countdown} & App. & DVFS & EtS + TtS & R &
  R \\
  %\bottomrule
\end{tabular}
\end{table}

Again, a variety of metrics are used to measure the efficacy of each proposed
approach, with the majority of works considering EtS and power consumption,
often in conjunction. 
Some works directly factor the monetary savings into their evaluation and use it
as a measure of the quality of their approach (identified with \$ in the
\emph{Evaluation} column).

In the case of energy efficiency, fewer solutions have been deployed on real
systems. This might be the case because dealing with energy is inherently more
complex than considering only power consumption, thus the proposed
approaches are generally less mature.
%% In particular, there are very few works
%% that consider also the financial impact of the proposed methods, while this is a
%% very important aspect. For example, reducing the energy consumption of a HPC
%% system could have a negative impact on its utilization; since supercomputers are
%% generally very costly investment, facility managers might be less motivated
%% towards energy efficient solutions if large portions of their expenses are
%% related to depreciation costs (w.r.t. electricity bills and operating costs).

%% Generally speaking, in the HPC context there are not so many works taking into
%% account different aspects such as economic impacts of power/energy saving
%% mechanisms and operating and investment costs of a whole facility; the typical
%% focus is mostly on single methods and technologies, without a holistic system
%% view. For example, very few works consider the problem of how to foster the
%% adoption of energy efficient approaches through dedicated pricing schemes (see
%% \cite{borghesi2018pricing}). In this area, a lot of knowledge could be borrowed
%% from the data centers and cloud providers field, thanks to the substantial
%% literature in the area
%% \cite{samadi2010optimal,sharma2012pricing,wang2014hierarchical,zhao2014dynamic}.
