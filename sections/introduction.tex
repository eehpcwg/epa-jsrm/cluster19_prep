Today's supercomputing centers face a number of challenges related to
the energy and power demands of modern supercomputers.  As centers
push toward exascale-class machines, the power requirements of a
single large machine are an important consideration in the planning
and deployment process.  Further, many supercomputing centers run
multiple large machines, such as during a transition period between
machine generations or to address multiple mission spaces, and these
multiple machines exacerbate energy and power challenges.
Finally, as the size and degree of parallelism of supercomputers
increases, machines can exhibit significant power fluctuations when
parallel jobs are submitted and completed by the system, and these
power fluctuations are increasingly an important consideration to both
the supercomputing center and the electricity service provider.

In 2016, the Energy Efficient High Performance Computing Working Group
(\EEHPCWG) formed a team specifically dedicated to studying topics
related to using job scheduling and resource management middleware
layers for managing energy and power in supercomputers.  During 2016
and 2017, this Energy and Power Aware Job Scheduling and Resource
Management (\EPAJSRM) team conducted a survey and subsequent series of
comprehensive interviews of leading-edge computing centers.  The
purpose of the survey and interviews was to assess the state of the
art in \EPAJSRM innovations and to report the findings to the broad
supercomputing community in an effort to drive adoption of these
techniques.  An initial paper that describes the survey modalities,
the motivation behind the survey, the center selection process, and an
overview of the questionnaire and site responses was published in
early 2018~\cite{maiterth2018}.  A follow-up paper that provides a
more detailed analysis of of the survey results was published in late
2018~\cite{daac2018}.

The current paper summarizes the entirety of our work in this space to
date and includes a discussion of future directions and recommendations
for techniques that we believe will be significant in coming years based
on our investigation.  Section~\ref{sec:job_schedulers_and_resource_managers}
describes at a high level how job schedulers and resource management
middleware work together to influence the energy and power consumption
of supercomputers.  Section~\ref{sec:survey_summary} reviews the survey,
and readers are referred to~\cite{maiterth2018} for a more in-depth
coverage, including a detailed description of the questions asked.
An in-depth analysis of the survey is presented in
Section~\ref{sec:survey_analysis}.  A review of Related Work, such as
academic research as well as vendor efforts, appears in
Section~\ref{sec:related_work}.  Finally, Section~\ref{sec:future}
describes future directions in energy and power aware resource
management techniques that are being considered in some centers and
Section~\ref{sec:recommendations} presents insights and
recommendations based on our research.
