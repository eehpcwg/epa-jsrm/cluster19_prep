The techniques involved in \EPAJSRM are continually evolving at a
rapid rate.  To that end, several centers have advanced their work
beyond that reported during the 2016-2017 survey and interview
process.  This section describes recent advances in \EPAJSRM
techniques made by Los Alamos National Laboratory and Sandia
National Laboratories in the Trinity supercomputer and at
LRZ with the SuperMUC-NG supercomputer.

\subsection{Trinity}
Several advanced power management capabilities are being developed as
part of the Trinity supercomputer project.  While Trinity is not itself
power constrained, it is anticipated that future supercomputers will be
and the size and scale of Trinity -- 42 petaflops peak and 8-10 MW power
consumption -- are being leveraged to help develop the required technologies
for managing supercomputer power usage intelligently.  Several target use cases
were identified, including:

\begin{itemize}
\item \emph{Large Job Startup} --- A large job may start and cause the
system's power usage to increase rapidly.  This is particularly a problem
coming out of system maintenance periods, when the system is previously idle
and large jobs are often started preferentially.  This may lead to violating
power ceiling or power ramp up contract terms with the local utility provider
and potentially trigger financial penalties.  Additionally, equipment may fail.

\item \emph{Early Job Termination} --- A large job may finish earlier than
projected or crash, leading to a sudden drop in power usage.  Similar to
large job startup, this may lead to contract violations and equipment failure.

\item \emph{Reduced Power Application Runs} --- For workloads that do not
require full power, allow individual jobs to be run with a reduced power usage
configuration.  Some applications, such as many memory-bandwidth bound applications,
 may not benefit from running at full power.  Alternatively, some users may be
willing to run at a reduced power level if it means their job will start
executing sooner, even if it results in lower performance while running.
%In both cases, an intelligent workload manager could reallocate the power
%savings to other applications while staying under the system-level power
%constraint.

%\item \textbf{Power Usage Accounting} -- provide an end of job report to users
%detailing the job's power usage characteristics.  This information may be useful
%for performance optimization purposes and for evaluating the full costs in return
%on investment studies.

\end{itemize}

Through a non-recurring engineering contract with Adaptive computing, support for
each of these use cases was implemented in the Moab/Torque workload manager.  To
support a system-level power floor and ceiling targets, the job scheduler was
modified to be aware of the power being consumed in the system and to launch jobs
accordingly.  The initial implementation used c-state control to maintain the
minimum power usage of each node, and hence the power floor.  Subsequent
implementations explored the use of a `power vampire` workload that would start
consuming power if power usage dropped below the desired floor.  Power ramp up
and ramp down were controlled using gradual p-state increases and c-state
decreases, respectively.  Last, job-level power templates were implemented to
allow users and administrators to create power management templates and apply
them to individual jobs, enabling reduced power application runs.

Future work is exploring how the workload manager and facility can interact
to run the system in a more energy efficient way.  For example, if the workload
manager can forecast power usage to the facility, this information could be used
to ramp up or down power generation and cooling capabilities accordingly.  This
sort of proactive approach has potential to be more efficient than the reactive
approaches currently used in practice.


\subsection{SuperMUC-NG}
\label{sec:future:supermuc-ng}
The next-generation SuperMUC-NG system at the Leibniz Supercomputing Centre
consists of approximately 6,500 compute nodes with an aggregate peak
performance of 27 petaflops.  As with the prior-generation SuperMUC system,
\EPAJSRM is an important focus area for the system with several new
capabilities planned:

\begin{itemize}

\item \emph{Multi-phase Applications} --- The original Load Leveler
implementation for SuperMUC only allowed one frequency setting per job.
SuperMUC-NG is being extended to support applications with multiple phases,
for example a memory-bound phase and a compute-bound phase, by allowing a
different frequency to be selected for each phase.

\item \emph{Automated Energy Tagging} --- To remove the burden on users to
manually specify energy tags, techniques are being developed to automatically
detect the characteristics of an application and generate an appropriate energy
tag. This may also improve accuracy by reducing the potential for user error.

\item \emph{Historical Job Database} --- A high-fidelity monitoring solution is
being deployed with SuperMUC-NG with the gathered information stored in an in-house
data center database. This information is then used to make scheduling
and job placement decisions.  Examples include placing high-power jobs nearby
absorption chillers to maximize waste heat collection and spreading out
high-power jobs in space or time to more evenly distribute power and cooling
load.

\end{itemize}

These new capabilities are technology development areas that will be pursued
over the lifetime of SuperMUC-NG.
