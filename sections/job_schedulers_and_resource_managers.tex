Throughout this paper we refer to two types of system software that we
specifically define here.  Job schedulers allow high-performance
computing users to efficiently share the computing resources that
comprise an HPC system.  Users submit batch jobs into one or more
batch queues that are defined within the job scheduler.  The job
scheduler examines the overall set of pending work waiting to run on
the computer and makes decisions about which jobs to place next onto
the computational nodes within the computer.  Generally speaking, the
job scheduler attempts to optimize some characteristic such as overall
system utilization or fast access to resources for some subset of
batch jobs within the computing center's overall workload.  The
various queues that are defined within the job scheduler may be
designated as having higher or lower priorities and may be restricted
to some subset of the center's users, thus allowing the job scheduler
to understand distinctions of importance of certain jobs within the
overall workflow.

To carry out its work, a job scheduler typically interacts with one or
more resource managers.  A resource manager is a piece of system
software that has privileged ability to control various resources
within a datacenter.  These resources can include things such as the
physical nodes that make up a high-performance computer's
computational resources; disks, disk channels, or burst buffer
hardware that comprise I/O resources; or network interfaces, network
channels, or switches that comprise interconnect resources.  For
example, a job scheduler might use resource management software to
configure the processing cores, memory, disk, and networking resources
within one or more computational nodes in accordance with the
requested resources for a specific batch job prior to launching that
job onto the allocated computational nodes.  Finally, in some cases,
resource management software might have the ability to actuate pieces
of the physical plant that are responsible for delivering electricity
to the datacenter or cooling the datacenter.

\begin{figure*}[htb]
\centering
\includegraphics[width=1.0\textwidth]{images/EPA_JSRM_solution.jpg}
\caption{Interactions among multiple components that make up a typical
Energy and Power Aware Job Scheduling and Resource Management (\EPAJSRM)
solution.}
\label{fig:EPA_JSRM_solution}
\end{figure*}

This paper considers the synthesized use of job schedulers and
resource managers to provide energy and power aware job scheduling and
resource management capabilities within a high-performance computing
datacenter.  Figure~\ref{fig:EPA_JSRM_solution} presents an overview
of the different components that may participate in such a solution.
As shown, depending on the complexity of the implementation, the tasks
of an \EPAJSRM solution can be divided into four functional categories
related to the monitoring and control of the energy and power consumed
by hardware resources as well as the associated availability of these
resources.  Energy and power \textit{monitoring} techniques complement
traditional resource management of processors, memory, nodes, disks,
and networks.  The \textit{control} of energy and power is heavily
dependent on telemetry sensors that are responsible for constantly
monitoring the activity of the system resources.  Examples of such
control techniques could range from reducing power consumption via
simple human-controlled actuation of processor dynamic voltage and
frequency settings to much more complex scenarios where the job
scheduler has detailed historical knowledge of job characteristics
and schedules multiple jobs simultaneously in a way that optimizes
for certain energy- or power-specific objectives.  Because system-wide
software agents like the job scheduler have access to details of a
supercomputing center's entire workload, and can potentially apply
advanced data analytics to the problem, they have the potential for
improving the energy and power consumption of supercomputers in ways
that are unlikely to be possible for human-controlled processes alone.
Accordingly, we expect that a trend in coming years will be to have
system-wide techniques play an increasing role in these endeavors.
Due to the previous absence of a general overview of \EPAJSRM
solutions employed, actual usage and benefits of these techniques
were only known within individual centers.
